#!/bin/bash
module=$(hwinfo --wlan | grep "Driver Modules" | cut -d " " -f5 | sed 's/"//g')
dev=$(hwinfo --wlan | grep "Device File" | cut -d " " -f5)
if [ "`which nmcli`" ]; then 
  echo "restarting network connection"
     /bin/sh -c 'nmcli networking off'
       systemctl stop NetworkManager
        ip link set $dev down
         modprobe -r $module
           sleep 1
            modprobe -v $module
             sleep .5
             ip link set $dev up
             sleep .5
            systemctl start NetworkManager
           /bin/sh -c 'nmcli networking on'
          /bin/sh -c 'nmcli r wifi off'
         sleep .5
       /bin/sh -c 'nmcli r wifi on'
elif [ "`which connmanctl`" ]; then
  echo "restarting network connection"
     /bin/sh -c 'connmanctl enable offline'
       systemctl stop connman
        ip link set $dev down
         modprobe -r $module
           sleep 1
            modprobe -v $module
             sleep .5
             ip link set $dev up
             sleep .5
            systemctl start connman
           /bin/sh -c 'connmanctl disable offline'
          /bin/sh -c 'connmanctl disable wifi'
         sleep .5
       /bin/sh -c 'connmanctl enable wifi'
fi
