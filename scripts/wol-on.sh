#!/bin/bash
interface=$(hwinfo --netcard | grep "Device File" | cut -d " " -f5 | grep e)
$(ethtool -s $interface wol g) &&
notify-send -i "network" 'Wake On Lan' 'turned on via ethtool'
rm -rf /etc/udev/rules.d/81-wol.rules
echo ACTION==\"add\", SUBSYSTEM==\"net\", NAME==\"e*\", RUN+=\"/usr/bin/ethtool -s \$name\ wol g\" >> /etc/udev/rules.d/81-wol.rules
